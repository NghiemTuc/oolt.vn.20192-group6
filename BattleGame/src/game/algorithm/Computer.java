package game.algorithm;

import java.util.ArrayList;
import java.util.Random;

import game.ui.Cell;
import game.ui.PanelMap;

public class Computer {
	private ArrayList<Position> listHit = new ArrayList<Position>();
	private Random r;
	private int hit;
	private ArrayList<String> possibility;
	private Position lastHit;
	private String direction;
	private PanelMap panelMap;
	private Position firstHit;
	
	public Computer(PanelMap panelMap)          
	{
		this.panelMap = panelMap;
		for(int i=0;i<PanelMap.DIM;i++) {
			for(int j=0;j<PanelMap.DIM;j++) {
				Position position = new Position(i,j);
				listHit.add(position);
			}
		}
		r = new Random();
		hit = 0;
	}

	public Cell cpuTurn( int choice )  
	{ 
		if (choice == 1)
		{			
			Ship ship = targetRandom();
			if(ship != null) 
			{
				if(ship.sunk()) 
					ship.setImageSunk();
				hit = 0;
				direction = null;
			}	
		}
		else {
			if(hit == 0)           
			{
				Ship ship = targetRandom();
				if(ship != null)                                     
				{
					hit++;
					if(ship.sunk()) { 
						ship.setImageSunk();
						removeContour(ship,choice);
						hit = 0;
						direction = null;
					}
					else {
						firstHit = lastHit;
						possibility = new ArrayList<String>();
						initPossibility();
					}
				}
			}
			else if (hit == 1) {
				Ship ship = targetHit1(); 
				if (ship != null) {  
					hit++;
					possibility = null;                
					if (ship.sunk()) { 
						ship.setImageSunk();                              
						removeContour(ship,choice);
						hit = 0;
						direction = null;
					}
				}
			}
			else{
				Ship ship = targetHit2();          
				if (ship != null) {
					hit++;
					if (ship.sunk()) {
						ship.setImageSunk();
						removeContour(ship,choice);
						hit = 0;
						direction = null;
					}
				} else {
					reverseDirection();                                     
				}
			}
		}
		return panelMap.getCell(lastHit);
	}
	
	private void remove_pos(Position p)
	{
		for(int i = 0 ; i < listHit.size() ; i++)
			if((listHit.get(i).getX() == p.getX()) && (listHit.get(i).getY() == p.getY())){
				listHit.remove(i);
				break;
			}
	}
	private boolean isListHit(Position p)
	{
		for(int i = 0 ; i < listHit.size() ; i++)
			if((listHit.get(i).getX() == p.getX()) && (listHit.get(i).getY() == p.getY()))
				return true;
		return false;
	}
	private Ship targetRandom() { 
		int random = r.nextInt(listHit.size());
		Position p = listHit.get(random);
		remove_pos(p);
		lastHit = p;
		Ship ship = panelMap.getShip(p);
		if(ship != null) {
			ship.setImageHit(p);
			
		}
		else {
			panelMap.setWater(p);
		}
		return ship;
	}
	private Ship targetHit1() {
		boolean wrong = true;
		Position p = null;
		do {
			int random = r.nextInt(possibility.size());       
			String move = possibility.remove(random);
			p = new Position(firstHit);
			p.move(move.charAt(0)); 
			direction = move;           
			if (panelMap.getCodeCell(p.getX(), p.getY()) != EnCode.WATER && isListHit(p)) {				
				wrong = false;
				remove_pos(p);
			}
		} while (wrong);
		lastHit = p;
		Ship ship = panelMap.getShip(p);
		if(ship != null) 
			ship.setImageHit(p);
		else {
			panelMap.setWater(p);
		}
		return ship;                      
	}
	private Ship targetHit2() {
		boolean isHit = false;
		Position p = new Position(lastHit);          
		do {
			p.move(direction.charAt(0));               

			if (p.outMap() || panelMap.getCodeCell(p.getX(), p.getY()) == EnCode.WATER) {                    
				reverseDirection();
			} else {
				if (panelMap.getCodeCell(p.getX(), p.getY()) != EnCode.HIT) {           
					isHit = true;
				}
			}
		} while (!isHit);
		remove_pos(p);
		lastHit = p;
		Ship ship = panelMap.getShip(p);
		if(ship != null) 
			ship.setImageHit(p);
		else {
			panelMap.setWater(p);
		}
		return ship;
	}
	private void removeContour(Ship sunk, int choice)   
	{
		int x = sunk.getX();
		int y = sunk.getY();
		int xe = sunk.getXe();
		int ye = sunk.getYe();
		if(!sunk.getDirection())     
		{
			if(y != 0)
			{
				Position p = new Position(x , y - 1);
				if(panelMap.getCodeCell(p) != EnCode.WATER) {
					if(choice == 3 ) 
						remove_pos(p);
					panelMap.setCodeCell(p, EnCode.WATER);
				}
			}
			if(ye != PanelMap.DIM -1)
			{
				Position p = new Position(xe , ye + 1);
				if(panelMap.getCodeCell(p) != EnCode.WATER) {
					if(choice == 3 ) 
						remove_pos(p);
					panelMap.setCodeCell(p, EnCode.WATER);
				}
			}
			if(x != 0)
			{
				for( int i = 0 ; i <= ye - y ; i++)
				{
					Position p = new Position(x-1 , y + i);
					if(panelMap.getCodeCell(p) != EnCode.WATER) {
						if(choice == 3 ) 
							remove_pos(p);
						panelMap.setCodeCell(p, EnCode.WATER);
					}
				}
			}
			if(xe != PanelMap.DIM - 1)
			{
				for( int i = 0 ; i <= ye - y ; i++)
				{
					Position p = new Position(x+1 , y + i);
					if(panelMap.getCodeCell(p) != EnCode.WATER) {
						if(choice == 3 ) 
							remove_pos(p);
						panelMap.setCodeCell(p, EnCode.WATER);
					}
				}
			}
		} 
		else {                           
			if(x != 0)                  
			{
				Position p = new Position(x -1 , y);
				if(panelMap.getCodeCell(p) != EnCode.WATER) {
					if(choice == 3 ) 
						remove_pos(p);
					panelMap.setCodeCell(p, EnCode.WATER);
				}
			}
			if(xe != PanelMap.DIM -1)
			{
				Position p = new Position(xe +1 , ye );
				if(panelMap.getCodeCell(p) != EnCode.WATER) {
					if(choice == 3 ) 
						remove_pos(p);
					panelMap.setCodeCell(p, EnCode.WATER);
				}
			}
			if(y != 0)
			{
				for( int i = 0 ; i <= xe - x ; i++)
				{
					Position p = new Position(x+i , y -1);
					if(panelMap.getCodeCell(p) != EnCode.WATER) {
						if(choice == 3 ) 
							remove_pos(p);
						panelMap.setCodeCell(p, EnCode.WATER);
					}
				}
			}
			if(ye != PanelMap.DIM - 1)
			{
				for( int i = 0 ; i <= xe - x ; i++)
				{
					Position p = new Position(x+i , y + 1);
					if(panelMap.getCodeCell(p) != EnCode.WATER) {
						if(choice == 3 ) 
							remove_pos(p);
						panelMap.setCodeCell(p, EnCode.WATER);
					}
				}
			}
		}
	}
	
	private void reverseDirection() {          
		if (direction.equals("N")) { 
			direction = "S";
		} else if (direction.equals("S")) {
			direction = "N";
		} else if (direction.equals("E")) {
			direction = "W";
		} else if (direction.equals("W")) {
			direction = "E";
		}
	}
	private void initPossibility()              
	{
		if(lastHit.getX() != 0)
		{
			possibility.add("N");
		}
		if(lastHit.getX() != PanelMap.DIM -1 )
		{
			possibility.add("S");
		}
		if(lastHit.getY() != 0 )
		{
			possibility.add("W");
		}
		if(lastHit.getY() != PanelMap.DIM -1 )
		{
			possibility.add("E");
		}
	}
	
	
}
