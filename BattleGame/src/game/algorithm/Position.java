package game.algorithm;

public class Position {
	public static final int DIM = 10;
	private int x;   
    private int y;
    public Position()
    {
    	
    }
    public Position(int x, int y)
    {
    	this.x = x;
    	this.y = y;
    }
    public Position(Position p){
		this.x = p.getX();
		this.y = p.getY();
	}
    public boolean equals(Object obj) {
    	Position position = (Position) obj;
    	if(position.getX() == this.getX() && position.getY() == this.getY())
    		return true;
    	return false;
    }
	public int getX() {
		return x;
	}
	public int getY() {
		return y;
	}
    public void setX(int x) {
		this.x = x;
	}
    public void setY(int y) {
		this.y = y;
	}
	public void move(char diretion){
		switch(diretion){
			case 'N':                   
				this.x--;
				break;
			case 'S':
				this.x++;
				break;
			case 'W':
				this.y--;
				break;
			case 'E':
				this.y++;
				break;
		}
	}
	
	public String toString(){
		char strY=(char)(y+65);
		return ""+(x+1)+" "+strY;
	}
	
	public boolean outMap(){
		if(x>=DIM||y>=DIM||x<0||y<0)
			return true;
		return false;
	}
}
