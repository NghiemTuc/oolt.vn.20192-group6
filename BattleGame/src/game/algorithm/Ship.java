package game.algorithm;

import java.util.ArrayList;

import javax.swing.ImageIcon;

import game.ui.Cell;

public class Ship {
	
	private ArrayList<Cell> cells = new ArrayList<Cell>();
	public ImageIcon getIcon(int size) {
		return new ImageIcon("image/ship" + size + ".png");
	}
	public void setCells(ArrayList<Cell> cells) {
		this.cells = cells;
	}
	public void addCell(Cell cell) {
		cell.setCode(EnCode.SHIP);
		this.cells.add(cell);
	}
	public void setImage() {
		String dir ;
		if(getDirection())
			dir = "Ver";
		else {
			dir = "Hor";
		}
		switch (getSize()) {
		case 1:
			cells.get(0).setImage("shipSize1" + dir + ".png");
			break;
		case 2:
		    cells.get(0).setImage("shipHead"+ dir +".png");
		    cells.get(1).setImage("shipFoot"+ dir +".png");	
			break;
		case 3:
			cells.get(0).setImage("shipHead"+ dir +".png");
		    cells.get(1).setImage("shipBody"+ dir +".png");
		    cells.get(2).setImage("shipFoot"+ dir +".png");
			break;
		case 4:
		    cells.get(0).setImage("shipHead"+ dir +".png");
		    cells.get(1).setImage("shipBody"+ dir +".png");
		    cells.get(2).setImage("shipBody"+ dir +".png");
		    cells.get(3).setImage("shipFoot"+ dir +".png");			
			break;

		default:
			break;
		}
	}
	
	public int getSize() {
		return cells.size();
	}
	public int getX() {
		return cells.get(0).getPosition().getX();
	}	
	public int getY() {
		return cells.get(0).getPosition().getY();
	}
	public Position getPosition() {
		Position position = new Position(getX(),getY());
		return position;
	}
	public int getXe() {
		return cells.get(getSize()-1).getPosition().getX();
	}	
	public int getYe() {
		return cells.get(getSize()-1).getPosition().getY();
	}
	public boolean getDirection() {
		if(getX() == getXe())
			return false; 
		return true; 
	}	
	public boolean isShip(Position p) {
		int x = p.getX();
		int y = p.getY();
		Cell firstCell = cells.get(0);
		Cell lastCell = cells.get(cells.size()-1);
		if(x <= lastCell.getPosition().getX() && x >= firstCell.getPosition().getX()
				&& y <= lastCell.getPosition().getY() && y >= firstCell.getPosition().getY())
			return true;
		return false;
	}
	public boolean sunk() {
		int dem = 0;
		for(Cell btn: cells) {
			if(btn.getCode() == EnCode.HIT)
				dem++;
		}
		return dem == getSize();
	}
	public boolean setImageHit(Position p) {
		for(Cell cell : cells) {
			if(cell.getPosition().equals(p)) {			
				cell.setImageHit();
				return true;
			}
		}
		return false;
	}
	public void setImageSunk() {
		for(Cell cell : cells) {
			cell.setImageSunk();
		}
	}   
}