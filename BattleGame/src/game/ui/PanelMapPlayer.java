package game.ui;

import java.util.ArrayList;

import game.algorithm.EnCode;
import game.algorithm.Position;
import game.algorithm.Ship;

public class PanelMapPlayer extends PanelMap{
	private static final long serialVersionUID = 1L;
	public PanelMapPlayer(String title) {
		super(title);
		int x = 1;
		int y = 1; 		
		for(int i = 0 ; i < DIM ; i++) {
			ArrayList<Cell> cells = new ArrayList<Cell>();
			for(int j = 0 ; j < DIM ; j++) {
				Cell btn = new CellPlayer(i, j);
				btn.setBounds(x,y,SIZE_BUTTON,SIZE_BUTTON);
				sea.add(btn);
				cells.add(btn);
				x += SIZE_BUTTON + 2; 
			}
			cellsMap.add(cells);
			x = 1; 
			y += SIZE_BUTTON + 2; 
		}		
	}
	public void setImageShip() {
		for(Ship ship : ships)
			ship.setImage();
	}
	public boolean importShip(Position p, int size, int direction) {
		
		if ((direction == 0) && ((p.getY() + size) > DIM)) {
			return false;
		}
		if ((direction == 1) && ((p.getX() + size) > DIM)) {
			return false;
		}
		boolean check;

		if (direction == 0)
			check = checkHorizontal(p, size);
		else
			check = checkVertical(p, size);

		if (!check)
			return false;
		Ship ship = new Ship();
		int x = p.getX();
		int y = p.getY();
		for (int i = 0; i < size; i++) {
			if (direction == 0) { 
//				setCodeCell(x, y + i, EnCode.SHIP);
				ship.addCell(getCell(x, y+i));
			}
			else {
//				setCodeCell(x + i, y, EnCode.SHIP);
				ship.addCell(getCell(x+i, y));
			}
		}
		ship.setImage();
		System.out.println(ship.getX()+" "+ship.getY()+" , "+ ship.getXe()+" "+ ship.getYe());
		ships.add(ship);		
		return true;
	}
	public Ship sunk(Position p) {
		Ship ship = getShip(p);
		if(ship ==  null)
			return null;
		for (int i = ship.getX(); i <= ship.getXe(); i++) {
			for (int j = ship.getY(); j <= ship.getYe(); j++) {
				if (getCodeCell(i, j) != EnCode.HIT) {
					return null;
				}
			}
		}
		return ship;
	}
//	public boolean setSunkShip(Position p) {
//		Ship ship = getShip(p);
//		if(ship == null) {
//			return false;
//		}
//		
//		ship.setImageSunk();
//		return true;
//	}
}
