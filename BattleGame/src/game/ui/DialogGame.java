package game.ui;

import java.awt.Color;
import java.awt.Font;
import java.awt.Toolkit;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class DialogGame extends JDialog{

	private static final long serialVersionUID = 1L;
	private JButton jbtNewGame, jbtExit;
	private JLabel jlbMessage1, jlbMessage2;

	public DialogGame(String message1, String message2) {
		
		setSize(432,244);
		setUndecorated(true);
		JPanel container = new PanelBackGround(Toolkit.getDefaultToolkit().createImage("image/gameOver.png"));

		jlbMessage1 = new JLabel(message1, JLabel.CENTER);
		jlbMessage1.setFont(new Font("Serif", Font.PLAIN, 40));
		jlbMessage1.setForeground(Color.RED);
		container.add(jlbMessage1);
		jlbMessage1.setBounds(0, 5, 432, 40);
		jlbMessage2 = new JLabel(message2, JLabel.CENTER);
		jlbMessage2.setFont(new Font("Serif", Font.PLAIN, 25));
		jlbMessage2.setForeground(Color.WHITE);
		container.add(jlbMessage2);
		jlbMessage2.setBounds(0, jlbMessage1.getHeight() + 15, 432, 40);

		jbtNewGame = new JButton("New game");
		container.add(jbtNewGame);
		jbtNewGame.setBounds(this.getWidth() * 2/10, this.getHeight() * 7/10, 110, 35);
		jbtNewGame.setBorder(null);
		jbtNewGame.setFocusPainted(false);

		jbtExit = new JButton("Exit");
		container.add(jbtExit);
		jbtExit.setBounds(this.getWidth() - (jbtNewGame.getX() + jbtNewGame.getWidth()), 
				jbtNewGame.getY(), jbtNewGame.getWidth(), jbtNewGame.getHeight());
		jbtExit.setBorder(null);
		jbtExit.setBorderPainted(false);
		jbtExit.setFocusPainted(false);
		add(container);
		container.setBounds(0, 0, this.getWidth(), this.getHeight());
		
		setLocationRelativeTo(null);
		setVisible(true);
	}

	public JButton getJbtNewGame() {
		return jbtNewGame;
	}
	
	public JButton getJbtExitGame() {
		return jbtExit;
	}
	
}
