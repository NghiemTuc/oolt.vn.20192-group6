package game.ui;

import java.util.ArrayList;

import javax.swing.ImageIcon;
import javax.swing.JLabel;

public class PanelMapCPU extends PanelMap{

	private static final long serialVersionUID = 1L;
	private JLabel volume = new JLabel();
	public PanelMapCPU(String title) {
		super(title);
		int x = 1;
		int y = 1; 		
		for(int i = 0 ; i < DIM ; i++) {
			ArrayList<Cell> cells = new ArrayList<Cell>();
			for(int j = 0 ; j < DIM ; j++) {
				Cell btn = new CellCPU(i, j);
				btn.setBounds(x,y,SIZE_BUTTON,SIZE_BUTTON);
				sea.add(btn);
				cells.add(btn);
				x += SIZE_BUTTON + 2; 
			}
			cellsMap.add(cells);
			x = 1; 
			y += SIZE_BUTTON + 2; 
		}		
		volume.setIcon(new ImageIcon("image/volume.png"));
		volume.setBounds(0,0, 50, 50);
		volume.setVisible(false);
		volume.setOpaque(false);
		add(volume);
	}
	public JLabel getVolume() {
		return volume;
	}
	
	public void setTargetFrame(boolean turn) {
		for(ArrayList<Cell> btns : cellsMap)
			for(Cell btn : btns)
				if(btn instanceof CellCPU) {
					CellCPU btnCPU = (CellCPU) btn;
					if(!turn)
						btnCPU.setTarget();
					else {
						btnCPU.removeTarget();
					}
				}
	}

	
}
