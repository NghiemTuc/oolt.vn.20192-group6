package game.ui;

public class CellPlayer extends Cell{
	
	private static final long serialVersionUID = 1L;

	public CellPlayer(int x, int y) {
		super(x,y);
		setCursor(CURSOR_DEFAULT);
	}
}
