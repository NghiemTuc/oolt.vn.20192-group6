package game.ui;

import java.awt.Toolkit;
import java.util.ArrayList;

import javax.swing.JLabel;

import game.algorithm.Ship;

public class ControlShip extends PanelBackGround {

	private static final long serialVersionUID = 1L;
	private ArrayList<JLabel> ships = new ArrayList<JLabel>();
	
	public ControlShip() {
		super(Toolkit.getDefaultToolkit()
				.createImage("image/battlePaper.png"));
		for (int i = 0; i < FrameSetUp.NUM_SHIP; i++) {
			JLabel ship = new JLabel();
			ships.add(ship);
			this.add(ship);
		}
		Ship ship = new Ship();

		ships.get(0).setIcon(ship.getIcon(4));
		ships.get(1).setIcon(ship.getIcon(3));
		ships.get(2).setIcon(ship.getIcon(3));
		ships.get(3).setIcon(ship.getIcon(2));
		ships.get(4).setIcon(ship.getIcon(2));
		ships.get(5).setIcon(ship.getIcon(2));
		ships.get(6).setIcon(ship.getIcon(1));
		ships.get(7).setIcon(ship.getIcon(1));
		ships.get(8).setIcon(ship.getIcon(1));
		ships.get(9).setIcon(ship.getIcon(1));

		ships.get(0).setBounds(25, 5, 135, 35);
		ships.get(1).setBounds(215, 5, 103, 35);
		ships.get(2).setBounds(375, 5, 103, 35);
		ships.get(3).setBounds(25, 40, 70, 35);
		ships.get(4).setBounds(105, 40, 70, 35);
		ships.get(5).setBounds(185, 40, 70, 35);
		ships.get(6).setBounds(265, 40, 47, 35);
		ships.get(7).setBounds(322, 40, 47, 35);
		ships.get(8).setBounds(379, 40, 47, 35);
		ships.get(9).setBounds(436, 40, 47, 35);		
	}
	
	public JLabel getShips(int index) {
		return ships.get(index);
	}
}