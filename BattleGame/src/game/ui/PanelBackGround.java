package game.ui;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Image;

import javax.swing.JPanel;

public class PanelBackGround extends JPanel{
	
	private static final long serialVersionUID = 1L;
	private Image image;	
	public PanelBackGround(Image img) {
		this.image = img;
		Dimension size = new Dimension(img.getWidth(null),img.getHeight(null));
		
		setSize(size);
		setLayout(null);
	}
	@Override
	public void paintComponent(Graphics g) {  
		g.drawImage(this.image,0,0,null);
	}
}
