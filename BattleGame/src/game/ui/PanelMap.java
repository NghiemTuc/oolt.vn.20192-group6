package game.ui;

import java.awt.Toolkit;
import java.util.ArrayList;
import java.util.Random;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;

import game.algorithm.EnCode;
import game.algorithm.Position;
import game.algorithm.Ship;

public class PanelMap extends JPanel{
	
	private static final long serialVersionUID = 1L;
	public static final int HEIGHT = 570;
	public static final int WIDTH = 630; 

	public static final int DIM = 10;
	public static final int SIZE_BUTTON = 48;
	
	protected ArrayList<ArrayList<Cell>> cellsMap = new ArrayList<ArrayList<Cell>>();
	protected ArrayList<Ship> ships = new ArrayList<Ship>();
	protected PanelBackGround sea;
	
	private JLabel label = new JLabel(); 
	
	public PanelMap(String title) {
		setSize(HEIGHT,WIDTH);
		setLayout(null);
		setOpaque(false);		
		label.setIcon(new ImageIcon("image/" + title + ".png"));
		label.setBounds(50, 0, 457, 54);
		add(label);
		
		sea = new PanelBackGround(Toolkit.getDefaultToolkit().createImage("image/sea.png"));
		sea.setBounds(34, 45, 550, 550);
		
		int x = 40;
		int y = 0; 
	
		JPanel grid = new JPanel(null);
		grid.setOpaque(false);
		grid.add(sea);
		
		for(int i = 0; i < DIM ; i ++ ) {
			JLabel hor = new JLabel();
			hor.setIcon(new ImageIcon("image/"+ (i+11) + ".png"));
			hor.setBounds(x,y,SIZE_BUTTON,SIZE_BUTTON);
			
			JLabel ver = new JLabel();
			ver.setIcon(new ImageIcon("image/"+ (i+1) + ".png"));
			ver.setBounds(y,x,SIZE_BUTTON,SIZE_BUTTON);
			
			grid.add(hor);
			grid.add(ver);
			x += SIZE_BUTTON + 2;
		}
	
		grid.setBounds(0,45,550,660);
		add(grid);
	}
	public void setTitle(String title) {
		label.setIcon(new ImageIcon("image/" + title + ".png"));
	}
	public void setCursor(boolean hand) {
		for(ArrayList<Cell> cells : cellsMap) {
			for(Cell cell: cells)
				cell.setCursor(hand);
		}
	}
	public Ship getShip(Position p) {
		for(Ship ship : ships) {
			if(ship.isShip(p))
				return ship;
		}
		return null;
	}
//	public ArrayList<ArrayList<Cell>> getCellsMap() {
//		return cellsMap;
//	}
	public Cell getCell(int x, int y) {
		return cellsMap.get(x).get(y);
	}
	public Cell getCell(Position p) {
		return getCell(p.getX(), p.getY());
	}
	public EnCode getCodeCell(int x, int y) {
		return getCell(x, y).getCode();
	}
	public EnCode getCodeCell(Position p) {
		return getCell(p).getCode();
	}
	public void setCodeCell(Position p, EnCode code) {
		getCell(p).setCode(code);
	}
	public PanelBackGround getSea() {
		return this.sea;
	}
	public void setUpImageShip(Ship ship, Position p, Boolean direction, int size) {
		int x = p.getX();
		int y = p.getY();
		if(!direction)  // Ver
			for(int i = 0; i< size ; i++) {
				Cell cell = getCell(x, y+i);
				ship.addCell(cell);
			}
		else 
			for(int i = 0; i< size ; i++) {
				Cell cell = getCell(x+i, y);
				ship.addCell(cell);
			}
		ships.add(ship);
		ship.setImage();
	}	
	public void createMapRandom() { 
		clear(); 
		Random r = new Random();
		for(int i = 0; i < 4; i++)
			for(int j = 0; j <= i; j++)
				importShipRandom(r, 4 - i);
	}
	public void clear() { 
		for(ArrayList<Cell> cells : cellsMap) {
			for(Cell cell : cells) {
				cell.setCode(EnCode.NIL);
				cell.setIcon(Cell.GRAY_ICON);
				cell.setCursor(true);
			}
		}
		ships.removeAll(ships);
	}
	public void importShipRandom(Random random, int size) {
		boolean check;
		int direction; 
		Position pos = new Position();
		do {
			check = true;
			direction = random.nextInt(2);  
			if (direction == 0) {
				pos.setY(random.nextInt(DIM - size + 1));
				pos.setX(random.nextInt(DIM));
			} else {
				pos.setY(random.nextInt(DIM));
				pos.setX(random.nextInt(DIM - size + 1));
			}
			if (direction == 0)
				check = checkHorizontal(pos, size);
			else
				check = checkVertical(pos, size);
		} while (!check);
		Ship ship = new Ship();
		int x = pos.getX();
		int y = pos.getY();
		for (int i = 0; i < size; i++) {
			if (direction == 0) 
				ship.addCell(getCell(x, y+i));		
			else 
				ship.addCell(getCell(x+i, y));
			
		}
		ships.add(ship);
	}
	public boolean checkVertical(Position p, int size) {
		int x = p.getX();
		int y = p.getY();
		if (x != 0)
			if (getCodeCell(x-1, y) == EnCode.SHIP)
				return false;
		if (x != DIM - size) 
			if (getCodeCell(x+size, y) == EnCode.SHIP)
				return false;
		for (int i = 0; i < size; i++) {
			if (y != 0)				
				if (getCodeCell(x+i, y-1) == EnCode.SHIP)
					return false;
			if (y != DIM - 1)
				if (getCodeCell(x+i, y+1) == EnCode.SHIP)
					return false;
			if (getCodeCell(x+i, y) == EnCode.SHIP)
				return false;
		}
		return true;
	}
	public boolean checkHorizontal(Position p, int size) {
		int x = p.getX();
		int y = p.getY();
		if (y != 0)
			if (getCodeCell(x, y-1) == EnCode.SHIP)
				return false;
		if (y != DIM - size)
			if (getCodeCell(x, y+size) == EnCode.SHIP)
				return false;
		for (int i = 0; i < size; i++) {
			if (x != 0)
				if (getCodeCell(x-1, y+i) == EnCode.SHIP)
					return false;
			if (x != DIM - 1)
				if (getCodeCell(x+1, y+i) == EnCode.SHIP)
					return false;
			if (getCodeCell(x, y+i) == EnCode.SHIP)
				return false;
		}
		return true;
	}	
	public void setWater(Position p) {
		getCell(p).setImageWater();
	}
//	public boolean isWaterCell(Position p) {
//		int x = p.getX();
//		int y = p.getY();
//		return getCodeCell(x,y) == EnCode.WATER;
//	}
//	public boolean isHitCell(Position p) {
//		int x = p.getX();
//		int y = p.getY();
//		return getCodeCell(x,y) == EnCode.HIT;
//	}
	
}

