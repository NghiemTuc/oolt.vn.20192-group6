package game.ui;

import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Random;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;

import game.algorithm.Position;

public class FrameSetUp extends JFrame implements ActionListener{
	
	private static final long serialVersionUID = 1L;

	public static final int NUM_SHIP = 10;
	
	private boolean finish = false;
	private int importShip = 0 ;
	private SetUpShip setUpShip = new SetUpShip();
	private PanelMap mapPanel = new PanelMapPlayer("manage");

	public FrameSetUp() {
		super("BattleShip - Set up Ship");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setResizable(false);
		setSize(900, 672);
		setIconImage(Toolkit.getDefaultToolkit().createImage("image/icon.png"));
		Dimension dimension = Toolkit.getDefaultToolkit().getScreenSize();
		int x = (int) ((dimension.getWidth() - this.getWidth()) / 2);
		int y = (int) ((dimension.getHeight() - this.getHeight()) / 2);
		this.setLocation(x, y);
		PanelBackGround container = new PanelBackGround(Toolkit.getDefaultToolkit().createImage("image/setup.jpg"));
		mapPanel.setBounds(25, 25, 600, 620);
		mapPanel.setCursor(true);
		container.add(mapPanel);		
		setUpShip.setBounds(580, 25, 280, 800);		
		container.add(setUpShip);		
		add(container);
		
		for(int i = 0 ; i<NUM_SHIP;i++) {
			for(int j = 0 ; j< NUM_SHIP; j ++) {
				mapPanel.getCell(i, j).addActionListener(this);
				mapPanel.getCell(i, j).setName(i+ " " + j);
			}
		}
		setUpShip.getRandom().addActionListener(this);
		setUpShip.getStart().addActionListener(this);
		setUpShip.getReset().addActionListener(this);

	}
	
	private void random() {
		if(importShip == NUM_SHIP) 
			reset();
		Random random = new Random();
		for(int i = 0; i< setUpShip.getLengthCounterShip(); i++) {
			int n = Integer.parseInt(setUpShip.getCounterShip(i).getName());
			for(int j =0; j < n; j++) 
				mapPanel.importShipRandom(random,setUpShip.getLengthCounterShip() -i);
		}
		importShip = NUM_SHIP;
		finish = true;
		setUpShip.getStart().setEnabled(true);
		for(int j = 0; j < setUpShip.getLengthSelectShip(); j++)
			setUpShip.getSelectShip(j).setEnabled(false);
		setUpShip.getDirection(0).setEnabled(false);
		setUpShip.getDirection(1).setEnabled(false);
		for(int j = 0; j < setUpShip.getLengthCounterShip(); j++) {
			setUpShip.getCounterShip(j).setIcon(new ImageIcon("image/0.png"));;
			setUpShip.getCounterShip(j).setName("0");
		}
		setUpShip.getSelectShip(0).setSelected(true);
		PanelMapPlayer panelMapPlayer = (PanelMapPlayer) mapPanel;
		panelMapPlayer.setImageShip();
		
	}
	
	private void reset() {
		mapPanel.clear();
		for(int i = 0; i < PanelMap.DIM; i++)
			for(int j = 0; j < PanelMap.DIM; j++) {
				mapPanel.getCell(i, j).setEnabled(true);
			}
		finish = false;
		setUpShip.getStart().setEnabled(false);
		for(int i = 0; i < setUpShip.getLengthSelectShip(); i++)
			setUpShip.getSelectShip(i).setEnabled(true);
		setUpShip.getDirection(0).setEnabled(true);
		setUpShip.getDirection(1).setEnabled(true);
		for(int j = 0; j < setUpShip.getLengthCounterShip(); j++) {
			setUpShip.getCounterShip(j).setName("" + (j+1));
			setUpShip.getCounterShip(j).setIcon(new ImageIcon("image/" + (j+1) +".png"));;
		}
			setUpShip.getSelectShip(0).setSelected(true);
			importShip = 0;
	}
	private void start() {
		FrameBattleShip battle = new FrameBattleShip(mapPanel);
		battle.setVisible(true);
		this.setVisible(false);
	}
	private void cell(String string) {
		if(finish) {
			return;
		}
		String[] out = string.split(" ");
		int x = Integer.parseInt(out[0]);
		int y = Integer.parseInt(out[1]);
		int size = -1;
		int counter = -1;
		int stt = -1;
		for(int i= 0 ; i < setUpShip.getLengthSelectShip() ; i++) {
			if(setUpShip.getSelectShip(i).isSelected()) {
				size = setUpShip.getLengthSelectShip() - i;
				counter = Integer.parseInt(setUpShip.getCounterShip(i).getName());
				stt = i;
				break;
			}
		}
		int direction = -1;
		for(int i= 0 ; i < setUpShip.getLengthDirection() ; i++) {
			if(setUpShip.getDirection(i).isSelected()) {
				direction = i;
				break;
			}
		}
		Position p = new Position(x,y);
		PanelMapPlayer mapPlayer = (PanelMapPlayer) mapPanel;
		boolean insert = mapPlayer.importShip(p, size, direction);
		if(insert) {
			importShip++;
			counter--;
			setUpShip.getCounterShip(stt).setIcon(new ImageIcon("image/"+counter+".png"));
			setUpShip.getCounterShip(stt).setName(""+counter);
			if(counter == 0) {
				setUpShip.getSelectShip(stt).setEnabled(false);
				for(int i= 0 ; i < setUpShip.getLengthSelectShip() ; i++) {
					if(!setUpShip.getSelectShip(i).isSelected() && setUpShip.getSelectShip(i).isEnabled()) {
						setUpShip.getSelectShip(i).setSelected(true);
						break;
					}
				}
				
			}
			if(importShip == NUM_SHIP) {
				finish = true;
				setUpShip.getStart().setEnabled(true);
				for(int i= 0 ; i < setUpShip.getLengthDirection() ; i++) {
					setUpShip.getDirection(i).setEnabled(false);
				}
			}
		}
	}
	@Override
	public void actionPerformed(ActionEvent e) {
		JButton cell = (JButton) e.getSource();
		String btnName = cell.getName();
		switch (btnName) {
			case "reset":
				reset();
				break;
			case "random":
				random();
				break;
			case "start":
				start();
				break;
			default:
				cell(btnName);
				break;
		}
	}
	
}
