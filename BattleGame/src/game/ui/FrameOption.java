package game.ui;

import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class FrameOption extends JFrame implements MouseListener{

	private static final long serialVersionUID = 1L;
	private JLabel start = new JLabel();
	private JLabel exit = new JLabel();
	private JLabel option = new JLabel();
	private JLabel ok = new JLabel();
	private JLabel levelLabel = new JLabel();
	private JLabel speechLabel = new JLabel();
	
	private FrameSetUp  frameSetUp = new FrameSetUp();
	
	public static int speech = 1;
	public static int level = 1;
	
	private final Cursor cursor = Cursor.getPredefinedCursor(Cursor.HAND_CURSOR);

	public FrameOption() {
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setResizable(false);
		setUndecorated(true);
		setSize(900, 506);
		setIconImage(Toolkit.getDefaultToolkit().getImage("image/icon.png"));
		Dimension dimension = Toolkit.getDefaultToolkit().getScreenSize();
		int x = (int) ((dimension.getWidth() - getWidth()) / 2);
		int y = (int) ((dimension.getHeight() - getHeight()) / 2);
		setLocation(x, y); 
		
		JPanel container = new JPanel(null);
		PanelBackGround bg = new PanelBackGround(Toolkit.getDefaultToolkit().createImage("image/option-min.png"));
		container.add(bg);
		bg.setBounds(0, 0, 900, 506);
		
		start.setIcon(new ImageIcon("image/start-o.png"));
		start.setBounds(290, 93, 320,80);
		start.setCursor(cursor);
		start.addMouseListener(this);
		start.setName("start");
		container.add(start,0);
		
		option.setIcon(new ImageIcon("image/option-o.png"));
		option.setBounds(290, 213, 320,80);
		option.setCursor(cursor);
		option.addMouseListener(this);
		option.setName("option");
		container.add(option,0);
		
		exit.setIcon(new ImageIcon("image/exit-o.png"));
		exit.setBounds(290, 333, 320,80);
		exit.setCursor(cursor);
		exit.addMouseListener(this);
		exit.setName("exit");
		container.add(exit,0);
		
		ok.setIcon(new ImageIcon("image/ok-o.png"));
		ok.setBounds(290, 333, 320,80);
		ok.setCursor(cursor);
		ok.addMouseListener(this);
		ok.setName("ok");
		container.add(ok,0);
		ok.setVisible(false);
		
		levelLabel.setIcon(new ImageIcon("image/level-" + level + ".png"));
		levelLabel.setBounds(200, 93, 500,80);
		levelLabel.setCursor(cursor);
		levelLabel.addMouseListener(this);
		levelLabel.setName("level");
		container.add(levelLabel,0);
		levelLabel.setVisible(false);
		
		speechLabel.setIcon(new ImageIcon("image/speech-" + speech + ".png"));
		speechLabel.setBounds(200, 213, 500,80);
		speechLabel.setCursor(cursor);
		speechLabel.addMouseListener(this);
		speechLabel.setName("speech");
		container.add(speechLabel,0);
		speechLabel.setVisible(false);
		
		add(container);
		setVisible(true);
	}
	

	@Override
	public void mouseClicked(MouseEvent e) {
		JLabel label = (JLabel) e.getSource();
		switch (label.getName()) {
		case "exit":
			System.exit(0);
			break;
		case "start":
			this.setVisible(false);
			this.frameSetUp.setVisible(true);
			break;
		case "option":
			this.start.setVisible(false);			
			this.option.setVisible(false);
			this.exit.setVisible(false);
			this.ok.setVisible(true);
			this.levelLabel.setVisible(true);
			this.speechLabel.setVisible(true);
			break;
		case "ok":
			this.start.setVisible(true);			
			this.option.setVisible(true);
			this.exit.setVisible(true);
			this.ok.setVisible(false);
			this.levelLabel.setVisible(false);
			this.speechLabel.setVisible(false);
			break;
		case "level":
			if(level == 1 || level == 2) {
				level++;
			}
			else {
				level = 1;
			}
			levelLabel.setIcon(new ImageIcon("image/level-" + level + ".png"));
			break;
		case "speech":
			if(speech == 1 || speech == 2) {
				speech++;
			}
			else {
				speech = 1;
			}
			speechLabel.setIcon(new ImageIcon("image/speech-" + speech + ".png"));
			break;
			
		}
		
	}

	@Override
	public void mousePressed(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}
	
}
