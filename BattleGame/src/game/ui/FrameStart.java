package game.ui;

import java.awt.Dimension;
import java.awt.Toolkit;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
// man truoc khi vao tran
public class FrameStart extends JFrame{

	private static final long serialVersionUID = 1L;

	public FrameStart() {
		super("Battaglia Navale - Pirate Edition");
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setResizable(false);
		this.setUndecorated(true); // Tao 1 khung khong co vien loai bo close,minimum
		this.setSize(600, 350);
		this.setIconImage(Toolkit.getDefaultToolkit().
				// Tookit --> doc file anh
				// getDefaultToolkit() --> Tra ve thong tin đo phan giai man hinh
				getImage("image/icon.png"));
				// get 1 Image tu path
		Dimension dimension = Toolkit.getDefaultToolkit().getScreenSize();
		int x = (int) ((dimension.getWidth() - this.getWidth()) / 2);
		int y = (int) ((dimension.getHeight() - this.getHeight()) / 2);
		this.setLocation(x, y); // Set vi tri cua frame (tinh goc trai tren)
		JPanel container = new JPanel(null);
		PanelBackGround splashPanel = new PanelBackGround( // Tao anh nen
				Toolkit.getDefaultToolkit().createImage("image/splashimage.png"));
		// Tao nen tu 1 hinh anh
		ImageIcon loadingIMG = new ImageIcon("image/loading.gif");
		// tao 1 icon tu 1 path hinh anh
		JLabel loadingLabel = new JLabel(loadingIMG);
		container.add(splashPanel);
		splashPanel.setBounds(0, 0, 600, 350);
		//set vị tri 1 component trong Panel neu layout=null vs (x,y) la toa do trai tren
		container.add(loadingLabel, 0);
		loadingLabel.setBounds(560, 310, 24, 24);
		this.add(container);
		this.setVisible(true);		
	}
}
