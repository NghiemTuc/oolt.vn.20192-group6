package game.ui;

import java.awt.Cursor;
import java.awt.Toolkit;

import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JRadioButtonMenuItem;
import javax.swing.SwingConstants;

public class SetUpShip extends PanelBackGround{

	private static final long serialVersionUID = 1L;

	public final Cursor cursor = Cursor.getPredefinedCursor(Cursor.HAND_CURSOR);
	private JRadioButtonMenuItem[] selectShip = new JRadioButtonMenuItem[4];
	private ButtonGroup radioButtonShip = new ButtonGroup();
	private JLabel[] counterShip = new JLabel[4];
	
	private JLabel[] xLabel = new JLabel[4];

	private JButton random = new JButton();
	private JButton reset = new JButton();
	private JButton start = new JButton();

	private JRadioButton[] direction = new JRadioButton[2];
	private ButtonGroup dirGroup = new ButtonGroup();
	private JLabel title = new JLabel();
	
	public SetUpShip() {

		super(Toolkit.getDefaultToolkit().createImage("image/managePanel.png"));
		setLayout(null);
		setOpaque(false);
		title.setIcon(new ImageIcon("image/managePanelLabel.png"));
		title.setHorizontalAlignment(SwingConstants.CENTER);
		title.setBounds(0,30,280,35);
		add(title);
		
		JPanel selectShipPanel = new JPanel(null);
		selectShipPanel.setOpaque(false);
		selectShipPanel.setBounds(30, 90, 200, 300);
		
		for(int i = 0 ; i< 4 ; i++) {
			selectShip[i] = new JRadioButtonMenuItem(new ImageIcon("image/ship"+ (4-i) + ".png"));
			counterShip[i] = new JLabel(new ImageIcon("image/"+(i+1)+".png"));
			counterShip[i].setName((i+1)+""); 
		}
		for(int i = 0 ; i < selectShip.length ; i++) {
			
			selectShip[i].setBounds(0, 25 + (i * 60), 160, 40);
			selectShip[i].setOpaque(false);
			selectShip[i].setBorderPainted(false);
			radioButtonShip.add(selectShip[i]);
			selectShipPanel.add(selectShip[i]);
			
			counterShip[i].setBounds(210, 110 + (i * 60), 35, 35);
			counterShip[i].setOpaque(false);
			
			add(counterShip[i]);
			
			xLabel[i] = new JLabel("x");
			xLabel[i].setBounds(205,125 + (i * 60),23,19);
			xLabel[i].setOpaque(false);
			add(xLabel[i]);			
		}
		
		selectShip[0].setSelected(true);
		add(selectShipPanel);
		
		direction[0] = new JRadioButton("Horizontal");
		direction[0].setBounds(0, 260, 105, 20);
		direction[0].setSelected(true); //Cai doc chon
		direction[0].setOpaque(false);
		direction[0].setFocusPainted(false); // Khong hien vien khi focus
		dirGroup.add(direction[0]); // them vao group
		selectShipPanel.add(direction[0]); // them vao Panel
		
		direction[1] = new JRadioButton("Vertical");
		direction[1].setBounds(110, 260, 105, 20);
		direction[1].setOpaque(false);
		direction[1].setFocusPainted(false);
		dirGroup.add(direction[1]);
		selectShipPanel.add(direction[1]);
		
		// Set button chuc nang
		//Random
		random.setIcon(new ImageIcon("image/random.png"));
		random.setBorder(null);
		random.setOpaque(false);
		random.setBorderPainted(false); // Khong ve border
		random.setContentAreaFilled(false);// Khong hien thi text
		random.setBounds(30, 380, 200, 30);
		random.setCursor(cursor);
		random.setFocusPainted(false);
		random.setName("random");
		// Khi hover
		random.setRolloverIcon(new ImageIcon("image/randomHover.png"));
		add(random);
		
		//Reset
		reset.setIcon(new ImageIcon("image/reset.png"));
		reset.setBorder(null);
		reset.setOpaque(false);
		reset.setBorderPainted(false);
		reset.setContentAreaFilled(false); 
		reset.setBounds(10, 500, 137, 102);
		reset.setCursor(cursor);
		reset.setFocusPainted(false);
		reset.setName("reset");
		// Khi hover
		reset.setRolloverIcon(new ImageIcon("image/resetHover.png"));
		add(reset);
		
		//Reset
		start.setIcon(new ImageIcon("image/start.png"));
		start.setBorder(null);
		start.setOpaque(false);
		start.setBorderPainted(false);
		start.setContentAreaFilled(false); 
		start.setBounds(150, 500, 137, 102);
		start.setCursor(cursor);
		start.setFocusPainted(false);
		start.setName("start");
		start.setEnabled(false); // Mac dinh khong duoc hover
		// Khi hover
		start.setRolloverIcon(new ImageIcon("image/startHover.png"));
		add(start);
		
	}

	public JButton getRandom() {
		return random;
	}

	public JButton getReset() {
		return reset;
	}

	public JButton getStart() {
		return start;
	}

	public JRadioButtonMenuItem getSelectShip(int i) {
		return selectShip[i];
	}

	public JLabel getCounterShip(int i) {
		return counterShip[i];
	}
	
	public JRadioButton getDirection(int i) {
		return direction[i];
	}
	
	public int getLengthSelectShip() {
		return selectShip.length;
	}
	public int getLengthCounterShip() {
		return counterShip.length;
	}
	public int getLengthDirection() {
		return direction.length;
	}
}