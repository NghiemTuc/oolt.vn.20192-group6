package game.ui;

import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.Timer;

import game.algorithm.Computer;
import game.algorithm.EnCode;
import game.algorithm.Position;
import game.algorithm.Ship;
import game.control.CPU;
import game.control.Control;
import game.control.Player;

public class FrameBattleShip extends JFrame implements ActionListener{

	private static final long serialVersionUID = 1L;
	
	public final Cursor cursorDefault = Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR);
		
	private CPU cpu;
	private Player player;
 
	private PanelBackGround panel = new PanelBackGround(Toolkit.getDefaultToolkit().createImage("image/battleImg.jpg"));
	
	private Computer computer;
	
	private DialogGame dialogGame;
	
	private boolean turn;
	
	private Timer time;	

	public FrameBattleShip(PanelMap mapPlayer) {

		setTitle("Battle Ship");
		setSize(1150,720);
		setIconImage(Toolkit.getDefaultToolkit().createImage("simage/icon.png"));
		Dimension dimension = Toolkit.getDefaultToolkit().getScreenSize();
		int x = (int) ((dimension.getWidth() - getWidth()) / 2);
		int y = (int) ((dimension.getHeight() - getHeight()) / 2);
		setLocation(x, y);
		setResizable(false);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		player = new Player("player", mapPlayer);
		cpu = new CPU("cpu");
		
		PanelMapPlayer playerMap = (PanelMapPlayer) mapPlayer;
		
		this.computer = new Computer(playerMap);

		player.getControlShip().setBounds(30, 595, 500, 80);
		add(player.getControlShip());
		
		cpu.getControlShip().setBounds(630, 595, 500, 80);
		add(cpu.getControlShip());
		
		player.getPanelMap().setBounds(0, 0, PanelMap.WIDTH, PanelMap.HEIGHT+100);		
		this.panel.add(player.getPanelMap());
		
		cpu.getPanelMap().setBounds(600, 0, PanelMap.WIDTH, PanelMap.HEIGHT+100);
		
		this.panel.add(cpu.getPanelMap());
		
		
		add(panel);
		
		turn = false; 
		cpu.setPanelTarget(turn);
		for(int i = 0 ; i < PanelMap.DIM ; i ++) {
			for(int j = 0 ; j < PanelMap.DIM; j++) {
				cpu.getCell(i, j).addActionListener(this);
				cpu.getCell(i, j).setName(i + " " + j);
			}
		}
		int k = FrameOption.speech;
		switch (k) {
		case 1:
			k = 4000;
			break;
		case 2:
			k = 2000;
			break;
		case 3:
			k = 1000;
			break;		
		}
		time = new Timer(k, new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				time.stop();
				if(player.getNumShip() == 0) {
					dialogGame = new DialogGame("You lose!", "Do you want to play again?");
					dialogGame.getJbtNewGame().addActionListener(new ActionListener() {
						
						@Override
						public void actionPerformed(ActionEvent e) {
							dialogGame.setVisible(false);
							setVisible(false);
							FrameOption option = new FrameOption();
							option.setVisible(true);							
						}
					});
					dialogGame.getJbtExitGame().addActionListener(new ActionListener() {
						
						@Override
						public void actionPerformed(ActionEvent e) {
							System.exit(0);							
						}
					});
				}else {
					Cell button = computer.cpuTurn(FrameOption.level);
					Position p = button.getPosition();
					player.paintTarget(p.getX()*50, p.getY()*50); 
					EnCode code = button.getCode();
					boolean flag = code != EnCode.WATER;
					if(code == EnCode.SUNK) {
						player.setNumShip();
						player.deleteShip(player.getPanelMap().getShip(p).getSize());
					}
					turn = false;
					cpu.setPanelTarget(turn);
					Control.hit(flag);
					if(flag) {
						time.start();
						turn = true;
						cpu.setPanelTarget(turn);
					}
				}
			}
		});
		
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if(turn)
			return;
		JButton source = (JButton)e.getSource();
		String[] outStrings = source.getName().split(" ");
		int x = Integer.parseInt(outStrings[0]);
		int y = Integer.parseInt(outStrings[1]);
		Position p = new Position(x,y);
		Ship ship = cpu.getPanelMap().getShip(p);
		Control.hit(ship != null);
		if(ship != null) {
			ship.setImageHit(p);
			if(ship.sunk()){
				cpu.setNumShip();
				cpu.deleteShip(ship.getSize());
				ship.setImageSunk();
				if(cpu.getNumShip() == 0) {
					dialogGame = new DialogGame("You win!", "Do you want to play again?");
					dialogGame.getJbtNewGame().addActionListener(new ActionListener() {
						
						@Override
						public void actionPerformed(ActionEvent e) {
							dialogGame.setVisible(false);
							setVisible(false);
							FrameOption option = new FrameOption();
							option.setVisible(true);
						}
					});
					dialogGame.getJbtExitGame().addActionListener(new ActionListener() {
						
						@Override
						public void actionPerformed(ActionEvent e) {
							System.exit(0);							
						}
					});
				}
			}
		}
		else {
			cpu.getPanelMap().setWater(p);
			time.start();
			turn = true;
			cpu.setPanelTarget(turn);
		}
		this.requestFocusInWindow();
	}
	
			
}
