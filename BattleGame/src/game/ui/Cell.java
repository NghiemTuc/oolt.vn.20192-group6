package game.ui;

import java.awt.Cursor;

import javax.swing.ImageIcon;
import javax.swing.JButton;

import game.algorithm.EnCode;
import game.algorithm.Position;

public class Cell extends JButton{

	private static final long serialVersionUID = 1L;
	public static final Cursor CURSOR_DEFAULT = Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR);
	public static final Cursor CURSOR_HAND = Cursor.getPredefinedCursor(Cursor.HAND_CURSOR);
	public static final int SIZE_BUTTON = 48;
	public static final ImageIcon GRAY_ICON = new ImageIcon("image/grayButtonOpaque.png");
	private Position p;
	private EnCode code;
	
	public Cell(Position p) {
		super(GRAY_ICON);
		this.p = new Position(p);
		setSize(SIZE_BUTTON, SIZE_BUTTON);
		setOpaque(true);
		setBorder(null);
		setBorderPainted(false);
		setContentAreaFilled(false);
		setName(p.getX() + " " + p.getY());
		code = EnCode.NIL;
	}

	public Cell(int x, int y) {
		super(GRAY_ICON);
		Position position = new Position(x, y);
		this.p = new Position(position);
		setSize(SIZE_BUTTON, SIZE_BUTTON);
		setOpaque(true);
		setBorder(null);
		setBorderPainted(false);
		setContentAreaFilled(false);
		setName(p.getX() + " " + p.getY());
		code = EnCode.NIL;
	}
	public boolean isHit() {
		return code == EnCode.HIT;
	}
	public boolean isSunk() {
		return code == EnCode.SUNK;
	}
	public EnCode getCode() {
		return code;
	}
	public void setCode(EnCode code) {
		this.code = code;
	}
	public Position getPosition() {
		return this.p;
	}
	public void setCursor(boolean hand) {
		if(hand) {
			setCursor(CURSOR_HAND);
		}else {
			setCursor(CURSOR_DEFAULT);
		}
	}	
	public void setImage(String url) {
		setIcon(new ImageIcon("image/"+ url));
	}
	public void setImageHit() {
		setEnabled(false);
		setCursor(CURSOR_DEFAULT);
		setDisabledIcon(new ImageIcon("image/fireButton.gif"));
		setCode(EnCode.HIT);
	}
	public void setImageWater() {
		setEnabled(false);
		setCursor(CURSOR_DEFAULT);
		setDisabledIcon(new ImageIcon("image/template.png"));
		setCode(EnCode.WATER);
	}
	public void setImageSunk() {
		setEnabled(false);
		setCursor(CURSOR_DEFAULT);
		setDisabledIcon(new ImageIcon("image/sunk.gif"));
		setCode(EnCode.SUNK);
	}
}
