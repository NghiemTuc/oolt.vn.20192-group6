package game.ui;

import javax.swing.ImageIcon;

public class CellCPU extends Cell{
	private static final long serialVersionUID = 1L;
	private static final ImageIcon target = new ImageIcon("image/target.png");

	public CellCPU(int x, int y) {
		super(x,y);
		setCursor(CURSOR_HAND);
	}
	public void setTarget() {
		if(this.isEnabled()) {
			setRolloverIcon(target);
		}
	}
	public void removeTarget() {
		if(this.isEnabled())
			setRolloverIcon(GRAY_ICON);
	}
}
