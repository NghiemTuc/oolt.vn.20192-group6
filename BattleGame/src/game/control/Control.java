package game.control;

import game.ui.Cell;
import game.ui.ControlShip;
import game.ui.PanelMap;
import game.ui.PanelMapCPU;

public class Control {
	private static final MP3 WATER_MP3 = new MP3("sound/water_splash.mp3");
	private static final MP3 HITTED_MP3 = new MP3("sound/sinking.mp3");
	private static final MP3 HIT_MP3 = new MP3("sound/strike.mp3");
	
	private int numShip = 10;
	protected static boolean sound = true;
	protected PanelMap panelMap;
	private ControlShip controlShip;
	
	public Control() {
		controlShip = new ControlShip();
	}
	
	public static void hit(boolean hited) {
		if(sound) {
			HIT_MP3.play();
			if(hited)
				HITTED_MP3.play();
			else {
				WATER_MP3.play();
			}
		}
	}
	public ControlShip getControlShip() {
		return controlShip;
	}
	public PanelMap getPanelMap() {
		return panelMap;
	}
	public int getNumShip() {
		return numShip;
	}
	public void setNumShip() {
		this.numShip -=1;
	}
	public Cell getCell(int x, int y) {
		return panelMap.getCell(x, y);
	}
	public void setPanelTarget(boolean turn) {
		if(panelMap instanceof PanelMapCPU) {
			PanelMapCPU cpu = (PanelMapCPU) panelMap;
			cpu.setTargetFrame(turn);
		}
	}
	public void deleteShip(int size) {
		switch(size) {
		case 4:
			controlShip.getShips(0).setEnabled(false);
			break;
		case 3:
			if(!controlShip.getShips(1).isEnabled())
				controlShip.getShips(2).setEnabled(false);
			else
				controlShip.getShips(1).setEnabled(false);
			break;
		case 2:
			if(!controlShip.getShips(3).isEnabled())
				if(!controlShip.getShips(4).isEnabled())
					controlShip.getShips(5).setEnabled(false);
				else
					controlShip.getShips(4).setEnabled(false);
			else
				controlShip.getShips(3).setEnabled(false);
			break;
		case 1:
			if(!controlShip.getShips(6).isEnabled())
				if(!controlShip.getShips(7).isEnabled())
					if(!controlShip.getShips(8).isEnabled())
						controlShip.getShips(9).setEnabled(false);
					else
						controlShip.getShips(8).setEnabled(false);
				else
					controlShip.getShips(7).setEnabled(false);
			else
				controlShip.getShips(6).setEnabled(false);
			break;
		default:
				break;
		}
	}
	
}
