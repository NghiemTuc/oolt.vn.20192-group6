package game.control;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.ImageIcon;
import javax.swing.JLabel;

import game.ui.Cell;
import game.ui.PanelMapCPU;

public class CPU extends Control {
	
	private JLabel volume = new JLabel();
	
	public CPU(String title) {
		super();
		panelMap = new PanelMapCPU(title);
		panelMap.setOpaque(false);
		panelMap.createMapRandom();
		volume = getVolume();		
		volume.setVisible(true);
		volume.setCursor(Cell.CURSOR_HAND);
		volume.addMouseListener(new MouseListener() {
			
			@Override
			public void mouseReleased(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mousePressed(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mouseExited(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mouseEntered(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mouseClicked(MouseEvent e) {
				setImageVolume();			
			}
		});
	}
	public JLabel getVolume() {
		PanelMapCPU cpu = (PanelMapCPU) panelMap;
		return cpu.getVolume();
	}
	public void setImageVolume() {
		if(panelMap instanceof PanelMapCPU) {
			PanelMapCPU cpuPanel = (PanelMapCPU) panelMap;
			if(sound) {
				cpuPanel.getVolume().setIcon(new ImageIcon("image/mute.png"));	
			}else {
				cpuPanel.getVolume().setIcon(new ImageIcon("image/volume.png"));	
			}
			sound = !sound;
		}		
	}	
//	public void setSunk(Ship shipControl) {
//		shipControl.setImageSunk();
//		deleteShip(shipControl.getSize());
//	}
}
