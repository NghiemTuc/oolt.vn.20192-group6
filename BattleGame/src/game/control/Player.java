package game.control;

import java.awt.Toolkit;

import javax.swing.JPanel;

import game.algorithm.Position;
import game.algorithm.Ship;
import game.ui.PanelBackGround;
import game.ui.PanelMap;
import game.ui.PanelMapPlayer;

public class Player extends Control{
	
	private JPanel targetPanel = new JPanel(null);
	private PanelBackGround target = new PanelBackGround(Toolkit.getDefaultToolkit().createImage("image/target.png"));
	
	public Player(String title, PanelMap panelMap) {
		super();
		this.panelMap = panelMap;
		this.panelMap.setTitle("player");
		targetPanel.setBounds(0, 0, 500, 500);
		targetPanel.setOpaque(false);
		panelMap.getSea().add(targetPanel);
		panelMap.setOpaque(false);
		PanelMapPlayer panelMapPlayer = (PanelMapPlayer) panelMap;
		panelMapPlayer.setImageShip();
	}	
	
	public void paintTarget(int x, int y) {
		target.setBounds(y, x, 50, 50);
		target.setVisible(true);
		targetPanel.add(target);
		targetPanel.repaint();
	}
	
	public void setSunk(Position p) {
		Ship ship = panelMap.getShip(p);
		if(ship == null)
			return;
		ship.setImageSunk();
		deleteShip(ship.getSize());
	}
}
