# Mini Project
## Đề tài : Xây dựng game BattleShip
---

### Mô tả dự án
-  Trò chơi có tên là BattleShip. Trong trò chơi người chơi sẽ sắp xếp thuyền trên biển sau đó phán đoán vị trí thuyền của đối phương để bắn hạ. Người chiến thắng là người bắn hạ tất cả thuyền trước tiên

- Yêu cầu:

-- Xây dựng hệ thống bản đồ trong trò chơi

-- Cho phép người chơi có thể tự thêm thuyền vào bản đồ hoặc thêm ngẫu nhiên

-- Xây dựng thuật toán cho Computer

-- Xây dựng đồ họa cho game ( thuyền, Frame,...)

-- Thêm âm thanh cho trò chơi

---
### Phân công nhiệm vụ

#### 1. Lương Đào Quang Anh (Email: aluong343@gmail.com)
- Viết class Ship, Position, Computer, EnCode, MP3 
- Xây dựng slide thuyết trình, báo cáo
- Làm Video thuyết trình
- Phân tích thiết kế package Algorithm ( Class diagramm )
- Phân tích biểu đồ Use-Case

#### 2. Nguyễn Văn Đức (Email: duc.nv176723@sis.hust.edu.vn)
- Xây dựng các class: DialogGame, ControlShip, FrameStart, FrameSetUp, FrameBattleShip, SetUpShip, PanelMap( các phương thức importShipRandom, createMapRandom, clear , checkVertical, checkHorizontal), PanelMapPlayer(phương thức  importShip)
- Phân tích thiết kế package Control ( Class diagramm )
- Xây dựng slide thuyết trình, báo cáo

#### 3. Hoàng Trung Hiếu (Email: hieu.jno1@gmail.com)
- Phân tích, thiết kế biểu đồ lớp (Class Diagram), biểu đồ Use-Case 
- Xây dựng các class : PanelMap (các phương thức còn lại ), PanelMapCPU, PanelMapPlayer (các phương thức còn lại ), Cell, CellCPU, CellPlayer, Control, CPU, Player, FrameOption, PanelBackGround
- Xây dựng slide thuyết trình, báo cáo

---
### Tài liệu tham khảo
1. https://docs.oracle.com/javase/8/docs/
2. Bài tập lớn có sự hình ảnh, ý tưởng từ source code :  https://github.com/tassoneroberto/battleship/tree/master/battleship
3. Sử dụng thư viện có sẵn để thiết kế class MP3 để thêm âm thanh tại link: http://www.java2s.com/Code/Jar/j/Downloadjl101jar.htm
4. Tham khảo cách thêm nhạc tại link: https://c5zone.forumotion.com/t7528-thu-thuat-phat-nhac-mp3-trong-java
